package co.com.sofka.punto02;

import org.springframework.boot.SpringApplication;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ChatBadWords {
    public static void main(String[] args) {
        SpringApplication.run(ChatBadWords.class, args);

        List<String> badwords = List.of("malo1","malo2","malo3","malo4","malo5","malo6","malo7","malo8","malo9","malo10");

        while (true){
            System.out.println("Ingrese un mensaje");

            Scanner scanner = new Scanner(System.in);
            String mensaje = scanner.nextLine();
            String[] palabrasMensaje = mensaje.split(" ");

            List<String> listaPalabrasMensaje = Arrays.asList(palabrasMensaje);
            Flux<String> fluxPalabrasMensaje = Flux.fromIterable(listaPalabrasMensaje);

            Flux<String> mensajeCorregido = fluxPalabrasMensaje.map(palabra->{
                if(badwords.contains(palabra))
                    return "****";
                return palabra;
            });

            mensajeCorregido.subscribe(palabra->System.out.print("> " + palabra));
            System.out.println();
        }
    }
}
